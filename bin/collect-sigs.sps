#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: GPL-3.0-or-later
#!r6rs

;; Collect signatures from an Akku archive.

(import
  (rnrs (6))
  (only (srfi :1 lists) append-map)
  (only (srfi :13 strings) string-prefix? string-suffix?)
  (srfi :19 time)
  (only (srfi :67 compare-procedures) >? default-compare)

  (akku lib manifest)
  (only (akku lib utils) path-join get-index-filename)
  (only (akku private compat) directory-list)
  (chibi match)
  (industria openpgp)
  (only (spells filesys) file-regular? file-directory?)
  (wak fmt))

(define (find-packages dir)
  (define (find-files realpath files)
    (define (recurse fn)
      (let ([realpath (path-join realpath fn)])
        (cond
          ((string-prefix? "." fn)
           '())
          ((and (file-regular? realpath) (string-suffix? ".akku" fn))
           (list realpath))
          ((file-directory? realpath)
           (find-files realpath (directory-list realpath)))
          (else
           '()))))
    (append-map recurse (list-sort string<? files)))
  (find-files dir (directory-list dir)))

(define (get-signature akku-file)
  (let ((sig (call-with-port (open-file-input-port (string-append akku-file ".sig"))
               get-openpgp-packet))
        (pkg (call-with-input-file akku-file
               read)))
    (cons (date->string
           (openpgp-signature-creation-time sig)
           "~Y-~m-~d")
          (match pkg
            [('package ('name name)
                       ('versions (('version version) . _)))
             (list name version)]))))

(let ((archive (find-packages (cadr (command-line)))))
  (fmt #t (pretty
           (list-sort (>? default-compare)
                      (map get-signature archive)))))
