#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018, 2019 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;; Static web site generation for akkuscm.org

(import
  (rnrs (6))
  (only (srfi :1 lists) last take)
  (only (srfi :13 strings) string-index)
  (only (srfi :67 compare-procedures) <? string-compare-ci)
  (only (akku private compat) open-process-ports putenv)
  (akku lib manifest)
  (only (akku lib utils) path-join application-data-directory
        url-join mkdir/recursive sanitized-name)
  (chibi match)
  (semver versions)
  (semver ranges)
  (wak htmlprag)
  (xitomatl alists))

(define (open-process cmd)
  (let-values (((to-stdin from-stdout from-stderr _process-id)
                (open-process-ports cmd (buffer-mode block)
                                    (native-transcoder))))
    (close-port to-stdin)
    (close-port from-stderr)
    from-stdout))

(define (write-shtml filename shtml)
  (display filename)
  (newline)
  (when (file-exists? filename)
    (delete-file filename))
  (call-with-output-file filename
    (lambda (p)
      (write-shtml-as-html shtml p))))

(define (template title nav main)
  `(*TOP*
    (*DECL* doctype html)
    (html (^ (lang "en"))
          (head (meta (^ (charset "utf-8")))
                (meta (^ (name "viewport") (content "width=device-width, initial-scale=1, shrink-to-fit=no")))
                (meta (^ (name "description") (content "Akku.scm - Scheme package manager")))
                (link (^ (rel "icon") (type "image/png") (href "/assets/img/favicon.png")))
                ;; (link (^ (rel "icon") (type "image/svg+xml") (href "/assets/img/favicon.svg")))
                (title ,title)
                ;; Bootstrap
                (link (^ (rel "stylesheet")
                         (href "https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css")
                         (integrity "sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB")
                         (crossorigin "anonymous")))
                (link (^ (href "/assets/css/akkuscm.css") (rel "stylesheet"))))
          (body
           (div (^ (class "d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow"))
                (h5 (^ (class "my-0 mr-md-auto font-weight-normal"))
                    (a (^ (class "navbar-brand") (href "/"))
                       (img (^ (alt "Akku.scm")
                               (src "/assets/img/akku-horizontal-30px.png")
                               (height "30")))))

                (nav (^ (class "my-2 my-md-0 mr-md-3"))
                     (a (^ (class "p-2 text-dark") (href "https://gitlab.com/akkuscm/akku/wikis/home"))
                        "Wiki")
                     (a (^ (class "p-2 text-dark") (href "/docs/manpage.html"))
                        "Documentation")
                     " "
                     (a (^ (class "p-2 text-dark") (href "/packages/"))
                        "Packages")
                     ;; TODO: https://duckduckgo.com/search_box
                     ))
           ,@(if nav `((nav ,nav)) '())
           (main (^ (role "main") (class "container"))
                 ,@main)
           (footer
            (^ (class "page-footer"))
            ;; Contact details
            (small "© 2019 Göran Weinholt" " · "
                   (a (^ (href "mailto:info@akkuscm.org"))
                      "Email")
                   " · "
                   (a (^ (href "https://webchat.freenode.net/?channels=akku"))
                      "Chat")
                   " · "
                   (a (^ (href "https://weinholt.se/tag/akku/1/"))
                      "Blog")
                   " · "
                   (a (^ (href "/privacy/"))
                      "Privacy")
                   " · "
                   (a (^ (href "https://gitlab.com/akkuscm/akku-web"))
                      "Source code")))
           ;; Scripts
           ;; (script (^ (src "https://code.jquery.com/jquery-3.3.1.slim.min.js")
           ;;            (integrity "sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E=")
           ;;            (crossorigin "anonymous")))
           ;; (script "window.jQuery || document.write('<script src=\"assets/js/vendor/jquery-slim.min.js\"><\\/script>')")
           ;; (script (^ (src "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js")
           ;;            (integrity "sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49")
           ;;            (crossorigin "anonymous")))
           ;; (script (^ (src "https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js")
           ;;            (integrity "sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T")
           ;;            (crossorigin "anonymous")))

           ))))

(define (make-site-index)
  (template
   "Akku.scm - Scheme package manager"
   #f
   `((h1 "Package management made easy")
     (p (^ (class "lead"))
        "Akku.scm is a language package manager for Scheme. It
grabs hold of code and vigorously shakes it until it behaves properly."
        (ul (li "One command to install everything needed for your project.")
            (li "Separately declare your dependencies and locked versions.")
            (li "Automatically convert R" (sup "7") "RS libraries for use in R" (sup "6") "RS projects.")
            (li "Audited build scripts for use with FFI libraries.")
            (li "Mirror of R" (sup "7") "RS libraries from Snow.")))
     (p
      (a (^ (class "btn btn-primary") (role "button")
            (href "https://gitlab.com/akkuscm/akku/releases"))
         "Download from GitLab")
      " -or- "
      (a (^ (class "btn btn-primary") (role "button")
            (href "https://github.com/weinholt/akku/releases"))
         "Download from GitHub"))
     ;; Demo
     (p (^ (class "text-center"))
        (script (^ (src "https://asciinema.org/a/259410.js")
                   (id "asciicast-259410")
                   (async "async"))))
     ;; Cards
     (p
      (div (^ (class "card-deck"))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Features")
                     (p (^ (class "card-text"))
                        (ul (li "Dependency solver")
                            (li "Cryptographic signing")
                            (li "Command line interface")
                            (li "Locked dependencies")
                            (li "Project-based work flow")))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Supported standards")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            ;; http://www.r6rs.org/ http://www.r7rs.org/
                            (li "R" (sup "6") "RS and R" (sup "7") "RS Scheme")
                            ;; https://semver.org/
                            (li "Semantic Versioning 2.0.0")
                            ;; https://spdx.org/
                            (li "SPDX license expressions")
                            ;; https://www.openpgp.org/
                            (li "OpenPGP signatures")))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "License")
                     (p (^ (class "card-text"))
                        "Akku.scm is distributed under the GNU General Public License version 3.0 or later."
                        (br)
                        (a (^ (href "http://www.gnu.org/licenses/gpl.html"))
                           (img (^ (src "/assets/img/gplv3-127x51.png")
                                   (alt "GPL-3.0-or-later")))))))))
     (p
      (div (^ (class "card-deck"))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Supported Schemes")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            (li (a (^ (href "https://www.scheme.com/")) "Chez Scheme" (sup "✻")))
                            (li (a (^ (href "http://synthcode.com/wiki/chibi-scheme")) "Chibi Scheme" (sup "‡")))
                            (li (a (^ (href "https://www.gnu.org/software/guile/")) "GNU Guile" (sup "✻")))
                            (li (a (^ (href "http://practical-scheme.net/gauche/")) "Gauche Scheme" (sup "‡")))
                            (li (a (^ (href "http://ikarus-scheme.org/")) "Ikarus Scheme"))
                            (li (a (^ (href "https://github.com/leppie/IronScheme")) "IronScheme"))
                            (li (a (^ (href "http://larcenists.org/")) "Larceny Scheme"))
                            (li (a (^ (href "https://scheme.fail/")) "Loko Scheme"))
                            (li (a (^ (href "http://mosh.monaos.org/files/doc/text/About-txt.html")) "Mosh Scheme"))
                            (li (a (^ (href "https://racket-lang.org/")) "Racket (plt-r6rs)"))
                            (li (a (^ (href "https://bitbucket.org/ktakashi/sagittarius-scheme/wiki/Home")) "Sagittarius Scheme"))
                            (li (a (^ (href "http://marcomaggi.github.io/vicare.html")) "Vicare Scheme"))
                            (li (a (^ (href "http://www.littlewingpinball.net/mediawiki/index.php/Ypsilon")) "Ypsilon Scheme"))
                        (small "(✻) Can run Akku  (‡) R" (sup "7") "RS only"))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Tested platforms")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            (li "Cygwin")
                            (li "FreeBSD")
                            (li "GNU/Linux")
                            (li "MSYS2")
                            (li "OpenBSD")
                            (li "macOS")
                            ))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Latest uploads")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            ,@(map (match-lambda
                                    [(date name version)
                                     `(li (a (^ (href ,(package-url name))
                                                (title ,(name->string name) " " ,version))
                                             ,(name->string name))
                                          (small " [" ,date "]"))])
                                   (take (call-with-input-file "archive-timestamps.scm" read)
                                         13)))))))))))

(define (make-privacy-page)
  (template
   "Akku.scm - Privacy policy"
   #f
   `((h1 "Privacy policy")
     (p "This page describes how the Akku client and web site handle personally
identifiable information.")

     (h2 "Command line client")
     (p "The akku command line client does not collect personally
identifiable information. Some subcommands of the client make network
requests on behalf of the user and the target of the request may in turn
use the data sent over the network:"
        (ul (li "The commands that make these requests are marked with a \"*\" in the
akku help output.")
            (li "Network requests are only made to the extent that
they are necessary to carry out the requested operations, e.g. to
download an updated package index, download a package, to publish a
package, etc.")))

     (h2 "Web site")
     (p "The akkuscm.org and related web sites handle personally identifiable information
in the following manner:"
        (ul (li "Web server requests are saved in standard log files.")
            (li "The web server logs are rotated weekly and are shredded after four rotations.")
            (li "The web server logs are used purely for anonymous
statistics and troubleshooting.")
            (li "The web site displays the names of authors of software.")))
     (p "Questions and requests related to Akku and privacy should be sent to "
        (i "privacy at this domain name") "."))))

(define (read-package-index index-filename) ;XXX: should be in akku
  (let ((packages (make-hashtable equal-hash equal?)))
    ;; Read packages from the index.
    (call-with-input-file index-filename
      (lambda (p)
        (let lp ()
          (match (read p)
            ((? eof-object?) #f)
            (('package ('name name)
                       ('versions version* ...))
             (hashtable-set! packages name
                             (make-package name (map parse-version version*)))
             (lp))
            (else (lp))))))             ;allow for future expansion
    packages))

(define (package-url pkg)
  (let ((pkg (if (package? pkg) (package-name pkg) pkg)))
    (url-join (url-join "/packages" (sanitized-name pkg)) "")))

(define (name->string x)
  (if (string? x)
      x
      (call-with-string-output-port
        (lambda (p) (display x p)))))

(define (quote-package-name x)
  (if (string? x)
      x
      (call-with-string-output-port
        (lambda (p)
          (display #\" p)
          (display x p)
          (display #\" p)))))

(define (compare-names x y)
  (define (strip-paren x)
    (if (string? x)
        x
        (let ((str
               (call-with-string-output-port
                 (lambda (p)
                   (display x p)))))
          (substring str 1 (- (string-length str) 1)))))
  (string-compare-ci (strip-paren x) (strip-paren y)))

(define (make-package-index idx)
  (let ((names (hashtable-keys idx)))
    (vector-sort! (lambda (x y) (<? compare-names x y)) names)
    (template
     "Akku.scm packages"
     #f
     `((table
        (^ (class "table table-hover table-striped"))
        (thead
         (tr (th (^ (scope "col")) "Name")
             (th (^ (scope "col")) "Latest version")
             (th (^ (scope "col")) "Synopsis")))
        (tbody
         ,@(map (lambda (name)
                  (let* ((pkg (hashtable-ref idx name #f))
                         (ver (last (package-version* pkg))))
                    `(tr (td (a (^ (href ,(package-url pkg))) ,(name->string name)))
                         (td ,(version-number ver))
                         (td ,@(or (version-synopsis ver)
                                   '((i "synopsis missing")))))))
                (vector->list names))))))))

(define (render-packages idx)
  (define (lock-html lock)
    (match (assq-ref lock 'location)
      [(('git remote-url))
       `((a (^ (href ,remote-url)) ,remote-url) " "
         (span (^ (class "badge badge-pill badge-primary")) "git")
         (br)
         (small ,(car (assq-ref lock 'revision))) (br)
         ,(cond ((assq-ref lock 'tag #f) =>
                 (lambda (tag) `(("Tag: " ,(car tag)))))
                (else '())))]
      [(('url url))
       `((a (^ (href ,url)) ,url) " "
         (span (^ (class "badge badge-pill badge-primary")) "url")
         (br)
         #;
         (small ,(car (assq-ref lock 'revision))) (br)
         )]))
  (define (pkg-html prev pkg next)
    (define ver (last (package-version* pkg)))
    (template
     (string-append "Akku.scm: " (name->string (package-name pkg)))
     `(ul (^ (class "nav justify-content-center"))
          (li (^ (class "nav-item"))
              (a ,(if prev
                      `(^ (class "nav-link") (href ,prev))
                      '(^ (class "nav-link disabled") (href "#")))
                 "« Previous"))
          (li (^ (class "nav-item pull-right"))
              (a ,(if next
                      `(^ (class "nav-link") (href ,next))
                      '(^ (class "nav-link disabled") (href "#")))
                 "Next »"))
          )
     `((h1 ,(name->string (package-name pkg)) " "
           (span (^ (class "badge badge-pill badge-primary"))
                 ,(version-number ver)))
       (h3 ,@(or (version-synopsis ver) '((i "synopsis missing"))))
       ,@(map (lambda (paragraph)
                (if (and (> (string-length paragraph) 0)
                         (char=? #\space (string-ref paragraph 0)))
                    `(pre ,paragraph)
                    `(p ,paragraph)))
              (or (version-description ver) '()))

       (pre (samp "$ akku install " ,(quote-package-name (package-name pkg)) "\n"
                  "$ .akku/env"))

       (p
        (div (^ (class "card-deck"))
             (div (^ (class "card"))
                  (div (^ (class "card-body"))
                       (h5 (^ (class "card-title")) "Authors")
                       (p (^ (class "card-text"))
                          ,@(map (lambda (author)
                                   (cond ((string-index author #\<) =>
                                          (lambda (i)
                                            `(p ,(substring author 0 i))))
                                         (else `(p ,author))))
                                 (version-authors ver)))))
             ,@(if (version-homepage ver)
                   `((div (^ (class "card"))
                          (div (^ (class "card-body"))
                               (h5 (^ (class "card-title")) "Homepage")
                               (p (^ (class "card-text"))
                                  (a (^ (href ,(version-homepage ver))
                                        (target "_blank"))
                                     ,(version-homepage ver))))))
                   '())

             (div (^ (class "card"))
                  (div (^ (class "card-body"))
                       (h5 (^ (class "card-title")) "License")
                       (p (^ (class "card-text"))
                          ,(version-license ver))))

             ,@(if (or (pair? (version-depends ver))
                       (pair? (version-depends/dev ver)))
                   `((div (^ (class "card"))
                          (div (^ (class "card-body"))
                               (h5 (^ (class "card-title")) "Dependencies")
                               (p (^ (class "card-text"))
                                  ;; FIXME: render dev dependencies
                                  ,@(apply
                                     append
                                     (map (match-lambda
                                           [(name range)
                                            `((div (^ (class "row"))
                                                   (div (^ (class "col"))
                                                        (a (^ (href
                                                               ,(package-url
                                                                 (hashtable-ref idx name #f))))
                                                           ,(name->string name)))
                                                   (div (^ (class "col"))
                                                        (span
                                                         (^ (title ,(semver-range->string
                                                                     (semver-range-desugar
                                                                      (string->semver-range range)))))
                                                         ,range))))])
                                          (version-depends ver)))))))
                   '())

             (div (^ (class "card"))
                  (div (^ (class "card-body"))
                       (h5 (^ (class "card-title")) "Source code")
                       (p (^ (class "card-text"))
                          ,@(lock-html (version-lock ver))))))
        ;; Disqus -- commented out due to lack of use
        #|
        (div (^ (id "disqus_thread")))
        ,(let ((PAGE-URL (string-append "https://akkuscm.org"
                                        (package-url pkg)))
               (PAGE-IDENTIFIER (string-append (package-url pkg))))
           `(script
             "var disqus_config = function () {"
             "this.page.url = '" ,PAGE-URL "';"
             "this.page.identifier = '" ,PAGE-IDENTIFIER "';"
             "};"
             "(function() {"
             "var d = document, s = d.createElement('script');"
             "s.src = 'https://akkuscm.disqus.com/embed.js';"
             "s.setAttribute('data-timestamp', +new Date());"
             "(d.head || d.body).appendChild(s);"
             "})();"))
        (noscript "Please enable JavaScript to view the "
                  (a (^ (href "https://disqus.com/?ref_noscript"))
                     "comments")
                  " powered by Disqus.")
        |#
        ))))
  (define (render-package prev pkg next)
    (let* ((dir (path-join "build/packages" (sanitized-name (package-name pkg))))
           (fn (path-join dir "index.html")))
      (mkdir/recursive dir)
      (write-shtml fn
                   (pkg-html (and prev (package-url prev))
                             pkg
                             (and next (package-url next))))))
  (let-values (((_ pkgs) (hashtable-entries idx)))
    (vector-sort! (lambda (x y)
                    (<? compare-names (package-name x) (package-name y)))
                  pkgs)
    (do ((i 0 (+ i 1)))
        ((= i (vector-length pkgs)))
      (render-package (and (> i 0) (vector-ref pkgs (- i 1)))
                      (vector-ref pkgs i)
                      (and (< i (- (vector-length pkgs) 1))
                           (vector-ref pkgs (+ i 1)))))))

(define (render-manpage filename manpage)
  (putenv "MANPAGE" manpage)
  (let ((manual
         (call/cc
           (lambda (return)
             (let lp ((shtml (html->shtml (open-process "groff -man -Thtml \"$MANPAGE\""))))
               (match shtml
                 [('body . x*) (return x*)]
                 [(tag . x*) (for-each lp x*)]
                 [_ #f]))))))
    (write-shtml filename
                 (template "Akku.scm - manual page"
                           #f
                           manual))))

(write-shtml "build/index.html" (make-site-index))

(mkdir/recursive "build/privacy")
(write-shtml "build/privacy/index.html" (make-privacy-page))

(mkdir/recursive "build/packages")

(let ((index (read-package-index
              (path-join (application-data-directory) "index.db"))))
  (write-shtml "build/packages/index.html"
               (make-package-index index))
  (render-packages index))

(mkdir/recursive "build/docs")
(render-manpage "build/docs/manpage.html"
                ".akku/src/akku/docs/akku.1") ;a bit presuming
